package com.felhr.serialportexample;

import android.os.Handler;
import android.os.Message;
import android.widget.Toast;

import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;

/*
 * This handler will be passed to UsbService. Data received from serial port is displayed through this handler
 */
public class MyHandler extends Handler {

    // ----------------------------------------------------------------
    // ---------------------- Variables Globales ----------------------
    // ----------------------------------------------------------------

    private final WeakReference<MainActivity> mActivity;
    private HexManager hexManager;
    public ArrayList<Byte> dataByte = new ArrayList();
    public int counter = 0;
    public int limitador = 10;
    private boolean isNumber = true;
    private int headerData = 0;


    // ----------------------------------------------------------------
    // ----------------------- Constructor Clase ----------------------
    // ----------------------------------------------------------------

    public MyHandler(MainActivity activity) {
        mActivity = new WeakReference<>(activity);
        hexManager = new HexManager();
    }

    // ----------------------------------------------------------------
    // ------------------- Metodos del Set y Get ----------------------
    // ----------------------------------------------------------------
    public void setIsNumber(boolean changes){
        isNumber = changes;
    }

    public void setHeaderData(int changes){
        headerData = changes;
    }
    // ----------------------------------------------------------------
    // --------------------- Metodos del Handler ----------------------
    // ----------------------------------------------------------------
    @Override
    public void handleMessage(Message msg) {

        switch (msg.what) {
            case UsbService.MESSAGE_FROM_SERIAL_PORT:
                byte[] dataMsgByte = (byte[]) msg.obj;
                //String tempHex = String.format("%02X", dataPrueba[0]);

                //mActivity.get().display.append("Hexadeximal Value:"+tempHex + "\n");
                //mActivity.get().display.append("Hex Value Principal:"+dataPrueba.toString() + "\n");
                //byte[] dataMsgByte = ((String) msg.obj).getBytes();
                //dataByte.add(dataPrueba[0]);

                for (int i = 0; i < dataMsgByte.length; i++) {
                    String tempHex = String.format("%02X", dataMsgByte[i]);
                    mActivity.get().display.append("Hexadeximal Value:"+tempHex + "\n");

                    if(tempHex.equals("7E")){
                        counter = 0;
                        limitador = 10;
                        dataByte = new ArrayList();
                    }
                    dataByte.add(dataMsgByte[i]);
                    counter++;

                    if(dataByte.size() == 6){
                        int BTCNT = dataByte.get(dataByte.size() - 1);
                        limitador += BTCNT - 2;
                    }

                }
                break;
            case UsbService.CTS_CHANGE:
                Toast.makeText(mActivity.get(), "CTS_CHANGE",Toast.LENGTH_LONG).show();
                break;
            case UsbService.DSR_CHANGE:
                Toast.makeText(mActivity.get(), "DSR_CHANGE",Toast.LENGTH_LONG).show();
                break;
        }

        if (counter == limitador-2){
            JSONObject json = hexManager.responseToJSON(dataByte, isNumber, headerData);
            mActivity.get().display.append("JSON values:"+json.toString());
        }
    }

    // ----------------------------------------------------------------
    // ---------------------- Metodos Auxiliares ----------------------
    // ----------------------------------------------------------------

    public void displayVolume(){
        byte[] dataByteArray = new byte[dataByte.size()];

        for (int i = 0; i < dataByte.size(); i++) {
            dataByteArray[i] = dataByte.get(i);
        }

        byte[] volumen = Arrays.copyOfRange(dataByteArray, 10, 18);
        Double valueVolume = ByteBuffer.wrap(volumen).getDouble();

        mActivity.get().display.append("Value Gross Gal: " + valueVolume + "\n");
        counter = 0;
        dataByte = new ArrayList<Byte>();
    }
}

